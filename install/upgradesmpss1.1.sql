-- --------------------------------------------------------

--
-- 表的结构 `smpss_exlog`
--

CREATE TABLE IF NOT EXISTS `smpss_exlog` (
  `exid` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `excredit` float NOT NULL,
  `exrmb` float NOT NULL,
  `goodsid` int(11) DEFAULT NULL,
  `extime` int(11) NOT NULL,
  UNIQUE KEY `exid` (`exid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------
--
-- 表的结构 `smpss_member_cache`
--

CREATE TABLE IF NOT EXISTS `smpss_member_cache` (
  `mid` int(11) NOT NULL COMMENT '用户ID',
  `buyfre` int(10) NOT NULL DEFAULT '0' COMMENT '购买次数Frequency',
  `returnfre` int(10) NOT NULL DEFAULT '0' COMMENT '退款次数',
  `exchangecredit` float NOT NULL DEFAULT '0' COMMENT '已兑换积分',
  `totalspanding` float NOT NULL DEFAULT '0' COMMENT '消费总额',
  `totalrefund` float NOT NULL DEFAULT '0' COMMENT '退款总额',
  UNIQUE KEY `mid` (`mid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户消费信息缓存';


-- --------------------------------------------------------
--
-- 表的结构 `增加仓库字段`
--
ALTER TABLE  `smpss_purchase` ADD  `store_id` INT( 10 ) NOT NULL DEFAULT  '0' AFTER  `cat_id`
